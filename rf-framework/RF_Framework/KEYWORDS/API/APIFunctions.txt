*** Settings ***
Resource          ../../SETTINGS/StandardLibrary.txt
Resource          ../../SETTINGS/AllResource.txt

*** Keywords ***
[A] Post Request Data
    [Arguments]    ${URL}    ${APIName}    ${DataJSON}
    KEYWORD STATUS PENDING    [A] Post Request Data
    Log To Console    URL --> ${URL}
    Log To Console    APIName --> ${APIName}
    Log To Console    DataJSON --> ${DataJSON}
    &{headers}    Create Dictionary    content-type=application/json    charset=UTF-8    user-agent=request
    Create Session    URL    ${URL}
    ${resp}    Post Request    URL    ${APIName}    data=${DataJSON}    headers=${headers}
    Log To Console    status --> ${resp.status_code}
    Log To Console    Response Json --> ${resp.json()}
    Comment    Return From Keyword    ${resp}
    KEYWORD STATUS END    None

[A] Get Request Data
    [Arguments]    ${URL}    ${AppName}    ${ParamName}    ${UserName}    ${Password}
    KEYWORD STATUS PENDING    [A] Get Request Data
    ${Date}    Get Current Date
    ${auth}    Create List    ${UserName}    ${Password}
    Create Session    URL    ${URL}    auth=${auth}
    ${resp}    Get Request    URL    ${AppName}    params=${ParamName}
    Log To Console    status --> ${resp.status_code}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log To Console    resp --> ${resp}
    Set Suite Variable    ${STEP_STATUS}    PASS
    Comment    Log To Console    URL --> ${URL}
    Comment    Log To Console    Api Name --> ${ApiName}
    Comment    Log To Console    Params --> ${Params}
    Comment    ${Auth}    Create List    admin    admin@1234
    Comment    &{headers}=    Create Dictionary    content-type=application/json    charset=UTF-8
    Comment    Create Session    URL    ${URL}    auth=${auth}
    Comment    ${resp}=    Get Request    URL    ${ApiName}    params=${params}    headers=${headers}
    Comment    Return From Keyword    ${resp}
    KEYWORD STATUS END    None
    [Return]    ${resp}

[A] Get Json Values And Convert Object
    [Arguments]    ${JsonString}    ${NodeName}
    ${JsonObject}    JSONLibrary.Convert String to JSON    ${JsonString}
    Log To Console    JSON --> ${JsonObject}
    ${value}=    JSONLibrary.Get Value From Json    ${JsonObject}    $..${NodeName}
    Log To Console    Value --> ${Value}
    Return From Keyword    ${value}

[A] Response Status Should Be Success
    [Arguments]    ${RespStatusCode}
    Run Keyword And Ignore Error    Should Be Equal As Strings    ${RespStatusCode}    200
